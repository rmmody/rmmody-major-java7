#!/usr/bin/env bash
#
# Release script of the Major mutation framework, which builds each component of
# framework and bundles all of its components.
#
# This script expects 1 argument -- the release version -- and checks that the
# release version matches the current version of the working directory.
#
# Requirements:
# - The environment variable JAVA_HOME must be set and exported.
#
# Usage:
# ./release.sh <version>
#
# Example:
# ./release.sh 1.3.0
#
set -e

VERSION=$1
if [ -z "$VERSION" ]; then
    echo "usage: $0 <version>"
    exit 1
fi

TAG="v${VERSION}"

BASE=$(cd $(dirname $0)/.. && pwd)
TMP=/tmp/build-major
ARCHIVE="$BASE/release/major-${VERSION}_jre7.zip"

# Verify version number
major=$(grep "release.major\s*=" $BASE/major.properties | cut -f2 -d'=' | tr -d ' ')
minor=$(grep "release.minor\s*=" $BASE/major.properties | cut -f2 -d'=' | tr -d ' ')
version_wd="$major.$minor"
[ "${version_wd}" != "${VERSION}" ] && \
    echo "Provided version ($VERSION) doesn't match working directory version ($version_wd)" && \
    exit 2

# Create and clean temporary build directory
rm -rf $TMP
mkdir -p $TMP
# Remove existing archive
rm -f $ARCHIVE

# Copy wrapper scripts, examples, and core libraries
rsync -a $BASE/release/major $TMP/

TMP_MAJOR_ROOT="$TMP/major"

die() {
    local comp=$1
    echo "Couldn't build: $comp!"
    exit 1
}

# Build javac
(cd $BASE/javac && ant -f make/build.xml clean build-javac && cp dist/lib/javac.jar $TMP_MAJOR_ROOT/lib/) \
    || die "javac"

# Build config
(cd $BASE/config && ant clean dist && mkdir $TMP_MAJOR_ROOT/config && cp dist/config.jar $TMP_MAJOR_ROOT/config/) \
    || die "config"

# Build mmlc
(cd $BASE/mmlc && ant clean dist && cp dist/mmlc.jar $TMP_MAJOR_ROOT/lib/) \
    || die "mmlc"

# Build ant
(cd $BASE/ant && ant clean jars && cp build/lib/*.jar $TMP_MAJOR_ROOT/lib/) \
    || die "ant"

# Build standalone
(cd $BASE/analyzer/standalone && ant clean jar && cp dist/standalone.jar $TMP_MAJOR_ROOT/lib/) \
    || die "standalone"
    
# Build manual
export MAJOR_HOME=$TMP_MAJOR_ROOT
(cd $BASE/manual && make clean && make && mkdir $TMP_MAJOR_ROOT/doc && cp major.pdf $TMP_MAJOR_ROOT/doc/) \
    || die "manual"


# Pre-compile all mml files
(cd $TMP_MAJOR_ROOT/mml && \
    for file in *.mml; do ../bin/mmlc $file; done \
)

# Set timestamp
find $TMP_MAJOR_ROOT -exec touch {} +

# Archive all files
(cd $TMP && zip -r $ARCHIVE .)
