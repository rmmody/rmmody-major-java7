package triangle;

import org.junit.Test;
import static org.junit.Assert.*;

import static triangle.Triangle.TriangleType;
import static triangle.Triangle.TriangleType.*;


/**
 * Test class for the Triangle implementation.
 */
public class TriangleTest {


	@Test
	public void test1() {
		Triangle t = new Triangle();
		TriangleType actual = Triangle.classify(1, 1, 1);
		TriangleType expected = EQUILATERAL;
		assertEquals(expected, actual);
	}


	@Test
	public void test2() {
		TriangleType actual = Triangle.classify(0, 2, 1);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test3() {
		TriangleType actual = Triangle.classify(3, 8, 9);
		TriangleType expected = SCALENE;
		assertEquals(expected, actual);
	}

	@Test
	public void test4() {
		TriangleType actual = Triangle.classify(2, 3, 5);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test5() {
		TriangleType actual = Triangle.classify(4, 4, 5);
		TriangleType expected = ISOSCELES;
		assertEquals(expected, actual);
	}

	@Test
	public void test6() {
		TriangleType actual = Triangle.classify(4, 5, 4);
		TriangleType expected = ISOSCELES;
		assertEquals(expected, actual);
	}

	@Test
	public void test7() {
		TriangleType actual = Triangle.classify(5, 4, 4);
		TriangleType expected = ISOSCELES;
		assertEquals(expected, actual);
	}


	@Test
	public void test8() {
		TriangleType actual = Triangle.classify(1, 1, 3);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}


	@Test
	public void test9() {
		TriangleType actual = Triangle.classify(2, 2, 2);
		TriangleType expected = EQUILATERAL;
		assertEquals(expected, actual);
	}


	@Test
	public void test10() {
		TriangleType actual = Triangle.classify(5, 4, 3);
		TriangleType expected = SCALENE;
		assertEquals(expected, actual);
	}


	@Test
	public void test11() {
		TriangleType actual = Triangle.classify(2, 0, 1);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test12() {
		TriangleType actual = Triangle.classify(1, 2, 0);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test13() {
		TriangleType actual = Triangle.classify(0, 0, 2);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test14() {
		TriangleType actual = Triangle.classify(0, 2, 0);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test15() {
		TriangleType actual = Triangle.classify(2, 0, 0);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test16() {
		TriangleType actual = Triangle.classify(0, 0, 0);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}


	@Test
	public void test17() {
		TriangleType actual = Triangle.classify(5, 2, 2);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test18() {
		TriangleType actual = Triangle.classify(2, 2, 5);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test19() {
		TriangleType actual = Triangle.classify(2, 5, 2);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test20() {
		TriangleType actual = Triangle.classify(5, 2, 4);
		TriangleType expected = SCALENE;
		assertEquals(expected, actual);
	}

	@Test
	public void test21() {
		TriangleType actual = Triangle.classify(5, 3, 2);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test22() {
		TriangleType actual = Triangle.classify(2, 5, 3);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test23() {
		TriangleType actual = Triangle.classify(2, 1, 5);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test24() {
		TriangleType actual = Triangle.classify(5, 1, 2);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test25() {
		TriangleType actual = Triangle.classify(2, 5, 1);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test26() {
		TriangleType actual = Triangle.classify(3, 4, 5);
		TriangleType expected = SCALENE;
		assertEquals(expected, actual);
	}

	@Test
	public void test27() {
		TriangleType actual = Triangle.classify(1, 3, 1);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test28() {
		TriangleType actual = Triangle.classify(3, 1, 1);
		TriangleType expected = INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test29() {
		TriangleType actual = Triangle.classify(2, 5, 4);
		TriangleType expected = SCALENE;
		assertEquals(expected, actual);
	}

	@Test
	public void test30() {
		TriangleType actual = Triangle.classify(0, 4, 4);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test31() {
		TriangleType actual = Triangle.classify(4, 4, 0);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test32() {
		TriangleType actual = Triangle.classify(4, 0, 4);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test33() {
		TriangleType actual = Triangle.classify(2, 2, 4);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test34() {
		TriangleType actual = Triangle.classify(2, 4, 2);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test35() {
		TriangleType actual = Triangle.classify(4, 2, 2);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test36() {
		TriangleType actual = Triangle.classify(-1, 2, 2);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test37() {
		TriangleType actual = Triangle.classify(2, -1, 2);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test38() {
		TriangleType actual = Triangle.classify(2, 2, -1);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test39() {
		TriangleType actual = Triangle.classify(3, 3, 7);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test40() {
		TriangleType actual = Triangle.classify(3, 7, 3);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

	@Test
	public void test41() {
		TriangleType actual = Triangle.classify(7, 3, 3);
		TriangleType expected = TriangleType.INVALID;
		assertEquals(expected, actual);
	}

}
