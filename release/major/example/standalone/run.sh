#!/bin/sh

MAJOR_HOME=$(cd "../.." && pwd)

rm -rf bin
mkdir -p bin

echo
echo "- Running Major with mutation"
echo "  (javac -XMutator=\"\$MAJOR_HOME/mml/tutorial.mml.bin\" -d bin src/triangle/Triangle.java)"
$MAJOR_HOME/bin/javac -XMutator="$MAJOR_HOME/mml/tutorial.mml.bin" -d bin src/triangle/Triangle.java

echo
echo "- Compiling test case (config.jar and junit.jar has to be on the classpath!)"
echo "  (javac -cp bin:\$MAJOR_HOME/config/config.jar:\$MAJOR_HOME/lib/junit-4.11.jar test/triangle/TriangleTest.java)"
$MAJOR_HOME/bin/javac -Xlint:unchecked -cp bin:$MAJOR_HOME/config/config.jar:$MAJOR_HOME/lib/junit-4.11.jar -d bin test/triangle/TriangleTest.java

echo
echo "- Building Mutants.jar"
echo "  (jar cf ../MutatedClass.jar triangle/Triangle.class triangle/Triangle\$TriangleType.class) "
echo " (jar cf ../Tests.jar triangle/TriangleTest.class) "
(cd bin; jar cf ../MutatedClass.jar triangle/Triangle.class triangle/Triangle\$TriangleType.class)
(cd bin; jar cf ../Test.jar triangle/TriangleTest.class)

echo
echo "- Executing the standalone analyzer"
echo "  (java -cp .:$MAJOR_HOME/lib/junit-4.11.jar:$MAJOR_HOME/config/config.jar:$MAJOR_HOME/lib/standalone.jar major.analyzer.standalone.main.Main /tmp/major/example/standalone/MutatedClass.jar /tmp/major/example/standalone/Test.jar /tmp/major/example/standalone/mutants.log)"
java -cp .:$MAJOR_HOME/lib/junit-4.11.jar:$MAJOR_HOME/config/config.jar:$MAJOR_HOME/lib/standalone.jar major.analyzer.standalone.main.Main /tmp/major/example/standalone/MutatedClass.jar /tmp/major/example/standalone/Test.jar /tmp/major/example/standalone/mutants.log

