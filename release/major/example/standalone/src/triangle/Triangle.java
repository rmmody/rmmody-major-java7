package triangle;

/**
 * An implementation that classifies triangles.
 */
public class Triangle {

    /**
     * This enum gives the type of the triangle.
     */
    public static enum TriangleType {
        INVALID, SCALENE, EQUILATERAL, ISOSCELES
    };
    
    /**
     * This static method does the actual classification of a triangle, given the lengths
     * of its three sides.
     */
    public static TriangleType classify(int a, int b, int c) {
        if (a <= 0 || b <= 0 || c <= 0) {
            return TriangleType.INVALID;
        }
        int trian = 0;
        if (a == b) {
            trian = trian + 1;
        }
        if (a == c) {
            trian = trian + 2;
        }
        if (b == c) {
            trian = trian + 3;
        }
        if (trian == 0) {
            if (a + b <= c || a + c <= b || b + c <= a) {
                return TriangleType.INVALID;
            } else {
                return TriangleType.SCALENE;
            }
        }
        if (trian > 3) {
            return TriangleType.EQUILATERAL;
        }
        if (trian == 1 && a + b > c) {
            return TriangleType.ISOSCELES;
        } else if (trian == 2 && a + c > b) {
            return TriangleType.ISOSCELES;
        } else if (trian == 3 && b + c > a) {
            return TriangleType.ISOSCELES;
        }
        return TriangleType.INVALID;
    }
}
