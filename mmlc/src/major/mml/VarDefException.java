package major.mml;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.TokenStream;

public class VarDefException extends RecognitionException {
    private String message;

    public VarDefException(TokenStream input, String message){
        super(input);
        this.message="Error: "+message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
