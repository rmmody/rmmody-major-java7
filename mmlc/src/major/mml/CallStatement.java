package major.mml;

import java.util.List;

import com.sun.tools.javac.mutation.operator.TreeMutationProvider;
import com.sun.tools.javac.mutation.operator.IMutationProvider.MutationOperator;

public class CallStatement implements Statement {
    private MutationOperator op;
    private boolean remove;


    public CallStatement(MutationOperator op, boolean remove){
        this.op = op;
        this.remove=remove;
    }

    @Override
    public boolean execute(TreeMutationProvider tree, List<String> flatname) {
        return tree.enableOperator(flatname, op, remove);
    }

    @Override
    public String toString() {
        return "CallStmt: "+op;
    }

}
