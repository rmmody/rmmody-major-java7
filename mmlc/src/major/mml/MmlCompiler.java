package major.mml;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.util.Properties;

import major.mml.antlr.MmlLexer;
import major.mml.antlr.MmlParser;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;

import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.file.JavacFileManager;
import com.sun.tools.javac.mutation.operator.TreeMutationProvider;
import com.sun.tools.javac.util.Context;

public class MmlCompiler {

    private static final String MAJOR_MMLC_DEBUG = "major.mmlc.debug";

    /**
     * Main method of the mml compiler
     * @param args:    1) input file
     *                 2) output file (optional)
     */
    public static void main(String[] args) {
        if(args.length<1 || args.length>2){
            System.err.println("Usage: java "+MmlCompiler.class.getName()+" <script-file> [<out-file>]");
            System.exit(1);
        }
        // hack for version option
        if(args[0].trim().equalsIgnoreCase("-version")){
            try {
                Properties props = new Properties();
                InputStream in = MmlCompiler.class.getResourceAsStream(MmlCompiler.class.getSimpleName()+".properties");
                props.load(in);
                in.close();
                System.out.println("mmlc "+props.getProperty("version"));
            }catch(Exception e){
                System.err.println("mmlc "+e.getMessage());
            }
            return;
        }

        String outfile = (args.length==1) ? outfile=args[0]+".bin" : args[1];

        try{
            ANTLRInputStream input=null;
            try{
                input = new ANTLRInputStream(new FileInputStream(args[0]));
            }catch(Exception e){
                System.err.println("Cannot open source file: "+args[0]);
                System.exit(1);
            }
            MmlLexer lexer = new MmlLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            MmlParser parser = new MmlParser(tokens);

            parser.script();
            if(parser.getSymbolTable().failed() || parser.error()){
                exitWithErr();
            }

            // Create symbol table for node transformation
            Context dummy = new Context();
            new JavacFileManager(dummy, true, null);
            Symtab symTab = Symtab.instance(dummy);

            // Transform the tree to correct operator representation
            TreeMutationProvider tree=parser.getTreeMutationProvider();

            if("true".equalsIgnoreCase(System.getProperty(MAJOR_MMLC_DEBUG))){
                System.out.println("Parse tree:\n"+tree);
            }

            // Attribute the entire tree
            if(!TreeMutationProvider.attributeNode(tree.getRootNode(), symTab)){
                exitWithErr();
            }

            if("true".equalsIgnoreCase(System.getProperty(MAJOR_MMLC_DEBUG))){
                System.out.println("Attributed tree:\n"+tree);
            }

            /*
             * Serialize tree and write mml output
             */
            try {
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(outfile));
                oos.writeObject(tree);
                oos.flush();
                oos.close();
            } catch (Exception e) {
                throw new RuntimeException("Cannot write mml file!", e);
            }

        }catch(Exception e){
            exitWithErr();
        }
    }

    private static void exitWithErr(){
        System.err.println("\nThe compilation process ended with errors!");
        System.exit(1);
    }
}
