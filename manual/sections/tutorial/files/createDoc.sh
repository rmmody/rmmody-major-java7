#!/bin/sh

if [ "$MAJOR_HOME" == "" ]; then
    echo "MAJOR_HOME must be set!"
    exit 1
fi

if [ ! -d "$MAJOR_HOME" ]; then
    echo "MAJOR_HOME is not a valid directory!"
    exit 1
fi

set -e

system="major\$"

echo "$system mmlc tutorial.mml tutorial.mml.bin" > ../genMml.session
$MAJOR_HOME/bin/mmlc tutorial.mml tutorial.mml.bin

echo "$system javac -XMutator=tutorial.mml.bin -d bin src/triangle/Triangle.java" > ../genMut.session
$MAJOR_HOME/bin/javac -XMutator=tutorial.mml.bin -d bin src/triangle/Triangle.java >> ../genMut.session

echo "$system head -3 mutants.log" > ../genResult.session
head -3 mutants.log >> ../genResult.session

echo "$system ant -DmutOp=\"=tutorial.mml.bin\" compile" > ../genMutAnt.session
$MAJOR_HOME/bin/ant clean init > /dev/null 2>&1
ant -DmutOp="=tutorial.mml.bin" -Dmajor=$MAJOR_HOME/bin/javac compile \
    | grep -v "Buildfile:" \
    | grep -v "init:" \
    | sed -e 's/source file to.*/source file to bin/' >> ../genMutAnt.session

echo "$system ant mutation.test" > ../runResult.session
$MAJOR_HOME/bin/ant compile.tests > /dev/null 2>&1
export TERM="dummy"
$MAJOR_HOME/bin/ant mutation.test \
    | grep -v "Buildfile:" >> ../runResult.session
