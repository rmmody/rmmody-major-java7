package com.sun.tools.javac.mutation.operator;

import java.util.ArrayList;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.util.Options;

public class DefaultMutationProvider implements IMutationProvider {
    private static final String ROOT = "";
    private TreeMutationProvider pathTree = new TreeMutationProvider(new TreeMutationProvider.TreeNode(ROOT));
    private List<String> flatnameRoot = new ArrayList<String>();

    private static final String[] OP_AOR = {"+", "-", "*", "/", "%"};
    private static final String[] OP_LOR = {"&", "|", "^"};
    private static final String[] OP_SOR = {"<<", ">>", ">>>"};
    private static final String[] OP_ORU = {"+", "-", "~"};

    public DefaultMutationProvider(Symtab tab, Options options){
        // set default operator mappings
        // AOR
        Set<String> setOp = new TreeSet<String>();
        for(String str : OP_AOR){
            setOp.add(str);
        }
        for(String str : OP_AOR){
            pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.BIN, str, setOp, true);
        }

        // LOR
        setOp.clear();
        for(String str : OP_LOR){
            setOp.add(str);
        }
        for(String str : OP_LOR){
            pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.BIN, str, setOp, true);
        }

        // SOR
        setOp.clear();
        for(String str : OP_SOR){
            setOp.add(str);
        }
        for(String str : OP_SOR){
            pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.BIN, str, setOp, true);
        }

        // ORU
        setOp.clear();
        for(String str : OP_ORU){
            setOp.add(str);
        }
        for(String str : OP_ORU){
            pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.UNR, str, setOp, true);
        }

        // Use only sufficient operators for ROR and COR

        // COR
        //
        // &&
        setOp.clear();
        for(String str : new String[]{"==", "LHS", "RHS", "FALSE"}){
            setOp.add(str);
        }
        pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.BIN, "&&", setOp, true);
        // ||
        setOp.clear();
        for(String str : new String[]{"!=", "LHS", "RHS", "TRUE"}){
            setOp.add(str);
        }
        pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.BIN, "||", setOp, true);

        // ROR
        //
        // >
        setOp.clear();
        for(String str : new String[]{">=", "!=", "FALSE"}){
            setOp.add(str);
        }
        pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.BIN, ">", setOp, true);
        // <
        setOp.clear();
        for(String str : new String[]{"<=", "!=", "FALSE"}){
            setOp.add(str);
        }
        pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.BIN, "<", setOp, true);
        // >=
        setOp.clear();
        for(String str : new String[]{">", "==", "TRUE"}){
            setOp.add(str);
        }
        pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.BIN, ">=", setOp, true);
        // <=
        setOp.clear();
        for(String str : new String[]{"<", "==", "TRUE"}){
            setOp.add(str);
        }
        pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.BIN, "<=", setOp, true);
        // ==
        setOp.clear();
        for(String str : new String[]{"<=", ">=", "FALSE", "LHS", "RHS"}){
            setOp.add(str);
        }
        pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.BIN, "==", setOp, true);
        // !=
        setOp.clear();
        for(String str : new String[]{"<", ">", "TRUE", "LHS", "RHS"}){
            setOp.add(str);
        }
        pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.BIN, "!=", setOp, true);

        // STD
        // Delete all types of supported statements by default
        setOp.clear();
        setOp.add("NO-OP");
        for (STD_TYPE type : STD_TYPE.values()) {
            pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.DEL, type.toString(), setOp, true);
        }

        // LVR
        // Replace numerical and boolean literals
        setOp.clear();
        setOp.add("ALL");
        pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.LIT, LIT_TYPE.NUMBER.toString(), setOp, true);
        pathTree.setMutationOperators(flatnameRoot, MutationOperatorType.LIT, LIT_TYPE.BOOLEAN.toString(), setOp, true);

        if(!TreeMutationProvider.attributeNode(pathTree.getRootNode(), tab)){
            throw new RuntimeException("Error while building default mutation provider");
        }

        // Check whether all operators should be enabled
        if(options.get(OPTION_NAME_MUTATOR+":ALL")!=null){
            for(MutationOperator op: MutationOperator.values()) {
                // check whether certain operators are disabled -> ignore the enabled ones
                if(options.get(OPTION_NAME_MUTATOR + ":-" + op.name())==null){
                    pathTree.enableOperator(flatnameRoot, op, false);
                }
            }
        }else{
            // Determine which specific operators have been enabled/disabled
            // If an operator is enabled and disabled at the same time,
            // the operator will be ENABLED, eventually!
            for(MutationOperator op: MutationOperator.values()) {
                if(options.get(OPTION_NAME_MUTATOR + ":" + op.name())!=null) {
                    pathTree.enableOperator(flatnameRoot, op, false);
                }
                else if(options.get(OPTION_NAME_MUTATOR + ":-" + op.name())!=null) {
                    pathTree.enableOperator(flatnameRoot, op, true);
                }
            }
        }
    }

    @Override
    public Collection<String> getMutationOperators(List<String> flatname,
            MutationOperatorType kind, String sym) {
        return pathTree.getMutationOperators(flatname, kind, sym);
    }

    @Override
    public boolean isOperatorEnabled(List<String> flatname, MutationOperator op) {
        return pathTree.isOperatorEnabled(flatname, op);
    }

    @Override
    public String toString() {
        return pathTree.toString();
    }

}
