package com.sun.tools.javac.mutation;

import com.sun.tools.javac.comp.AttrContext;
import com.sun.tools.javac.comp.Env;

public interface IMutator {
    /**
     * @return the current mutant counter
     */
    public int getMutationCounter();

    /**
     *
     * @param env the current environment with the class to be mutated
     */
    public void mutateTree(Env<AttrContext> env);
}
