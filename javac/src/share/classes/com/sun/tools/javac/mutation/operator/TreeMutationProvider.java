package com.sun.tools.javac.mutation.operator;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symbol.OperatorSymbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.mutation.operator.TreeMutationProvider.TreeNode.NodeEntry;

/*
 * Class for a complete path tree that holds the root node
 */
public class TreeMutationProvider implements IMutationProvider, Serializable{
    private static final long serialVersionUID = 1L;

    private final static Set<String> specialOperators = new TreeSet<String>(Arrays.asList(new String[]{"FALSE", "TRUE", "LHS", "RHS"}));

    private TreeNode root;

    public TreeMutationProvider(TreeNode root){
        assert (root!=null);
        this.root=root;
    }

    public TreeNode getRootNode(){
        return root;
    }

    @Override
    public Collection<String> getMutationOperators(List<String> flatname, MutationOperatorType kind, String sym) {
        TreeNode node = root;
        Set<String> opSet = new TreeSet<String>();

        // Add all operators of root node
        opSet.addAll(node.getMutOperators(kind, sym));
        // traverse tree according to flatname and add all operators of all children
        // -> the resulting set is the union of all operators
        for(String str : flatname){
            node=node.getChild(str);
            if(node==null) break;
            if(node.getMutOperatorMap(kind)!=null && node.getMutOperatorMap(kind).containsKey(sym) && node.getMutOperatorMap(kind).get(sym).replace){
                opSet=new TreeSet<String>();
            }
            opSet.addAll(node.getMutOperators(kind, sym));
        }
        return opSet;
    }

    @Override
    public boolean isOperatorEnabled(List<String> flatname, MutationOperator op){
        TreeNode node = root;
        boolean isEnabled = root.isOperatorEnabled(op);

        for(String str : flatname){
            node=node.getChild(str);
            if(node==null) break;
            // omit this node since 'op' is not specified here
            if(!node.isOperatorEnabled(op) && !node.isOperatorDisabled(op)) continue;
            isEnabled |= node.isOperatorEnabled(op);
            // reset if it is disabled
            if(node.isOperatorDisabled(op)) isEnabled=false;
        }
        return isEnabled;
    }

    public boolean enableOperator(List<String> flatname, MutationOperator op, boolean remove){
        if(!remove && isOperatorEnabled(flatname, op)){
            System.err.println("Warning: Operator "+op+" has already been enabled for "+flatname);
        }
        if(remove && !isOperatorEnabled(flatname, op)){
            System.err.println("Warning: Operator "+op+" has already been disabled for "+flatname);
        }
        TreeNode node = root;
        for(String str : flatname){
            if(node.getChild(str)==null){
                node.addChild(new TreeNode(str));
            }
            node = node.getChild(str);
        }
        if(remove){
            return node.disableOperator(op);
        }else{
            return node.enableOperator(op);
        }
    }

    public boolean setMutationOperators(List<String> flatname, MutationOperatorType kind, String sym, Set<String> ops, boolean replace){
        TreeNode node = root;
        for(String str : flatname){
            if(node.getChild(str)==null){
                node.addChild(new TreeNode(str));
            }
            node = node.getChild(str);
        }
        return node.putMutOperators(kind, sym, ops, replace);
    }

    @Override
    public String toString() {
        if(root==null) return "null";
        return root.toString();
    }

    /*
     * Innerclass for tree nodes
     */
    public static class TreeNode implements Serializable{
        public static class NodeEntry implements Serializable{
            private static final long serialVersionUID = 1L;
            public NodeEntry(Collection<String> opList, boolean replace){
                this.opList=opList;
                this.replace=replace;
            }
            public final Collection<String> opList;
            public final boolean replace;
        }
        private static final long serialVersionUID = 1L;

        private String indent="";
        private String name;
        private TreeNode parent = null;
        private Map<String, TreeNode> children = new HashMap<String, TreeNode>();
        private Map<MutationOperatorType, Map<String, NodeEntry>> mutationMap = new HashMap<MutationOperatorType, Map<String, NodeEntry>>();

        private Set<MutationOperator> enabledOperators = new TreeSet<MutationOperator>();
        private Set<MutationOperator> disabledOperators = new TreeSet<MutationOperator>();

        public TreeNode(String name){
            this(name, null, null);
        }

        public TreeNode(String name, TreeNode parent){
            this(name, parent, null);
        }

        public TreeNode(String name, TreeNode parent, Set<TreeNode> children){
            this.name=name;
            this.parent=parent;

            if(children==null) return;

            for(TreeNode node : children){
                this.children.put(node.name, node.setParent(this));
            }
        }

        /*
         * Enable a certain operator for this node
         */
        public boolean enableOperator(MutationOperator op){
            if(disabledOperators.contains(op)){
                System.err.println("Error: Operator "+op+" has been enabled and disabled for "+name);
                return false;
            }
            enabledOperators.add(op);

            return true;
        }

        /*
         * Disable a certain operator for this node
         */
        public boolean disableOperator(MutationOperator op){
            if(enabledOperators.contains(op)){
                System.err.println("Error: Operator "+op+" has been enabled and disabled for "+name);
                return false;
            }
            disabledOperators.add(op);

            return true;
        }

        /*
         * Check if a certain mutation operator is enabled for this node
         */
        public boolean isOperatorEnabled(MutationOperator op) {
            return enabledOperators.contains(op);
        }

        /*
         * Check if a certain mutation operator is disabled for this node
         */
        public boolean isOperatorDisabled(MutationOperator op) {
            return disabledOperators.contains(op);
        }

        public boolean putMutOperators(MutationOperatorType kind, String sym, Set<String> operators, boolean replace){
            // clone arguments
            Set<String> ops = new TreeSet<String>(operators);
            if(!mutationMap.containsKey(kind)){
                Map<String, NodeEntry> mutMap;
                mutationMap.put(kind, mutMap=new TreeMap<String, NodeEntry>());
                mutMap.put(sym.toString(), new NodeEntry(ops, replace));
            }else if(!mutationMap.get(kind).containsKey(sym)){
                mutationMap.get(kind).put(sym, new NodeEntry(ops, replace));
            }else{
                if(replace){
                    System.err.println("Warning: Overriding replacement list for operator "+sym+" in <"+name+">");
                    mutationMap.get(kind).put(sym.toString(), new NodeEntry(ops, replace));
                }else{
                    int expected = mutationMap.get(kind).get(sym).opList.size()+ops.size();
                    mutationMap.get(kind).get(sym).opList.addAll(ops);
                    if(mutationMap.get(kind).get(sym).opList.size()!=expected){
                        System.err.println("Warning: Redundancy in replacement list for operator "+sym+" in <"+name+">");
                    }
                }
            }
            return true;
        }

        public Map<String, NodeEntry> getMutOperatorMap(MutationOperatorType kind){
            return mutationMap.get(kind);
        }

        public Collection<String> getMutOperators(MutationOperatorType kind, String sym){
            if(!mutationMap.containsKey(kind)) return Collections.emptySet();
            if(!mutationMap.get(kind).containsKey(sym)) return Collections.emptySet();
            return mutationMap.get(kind).get(sym).opList;
        }

        public TreeNode addChild(TreeNode child){
            children.put(child.name, child.setParent(this));
            return this;
        }

        public TreeNode getChild(String name){
            return children.get(name);
        }

        public Iterable<TreeNode> getChildren(){
            return children.values();
        }

        public TreeNode getParent(){
            return parent;
        }

        public TreeNode setParent(TreeNode parent){
            this.parent=parent;

            return this;
        }

        @Override
        public String toString() {
            TreeNode tmpParent = parent;
            while(tmpParent!=null){
                // TODO Ugly hack!
                indent+="   ";
                tmpParent=tmpParent.parent;
            }

            StringBuffer buf = new StringBuffer(256);
            buf.append(indent+(parent==null?"":"|-")+name+"{\n");
            for(MutationOperatorType kind : MutationOperatorType.values()){
                if(mutationMap.containsKey(kind)){
                    buf.append(indent+"  "+kind.name()+":\n");
                    for(String sym : mutationMap.get(kind).keySet()){
                        buf.append(indent+"    "+(mutationMap.get(kind).get(sym).replace ? "!" : "+"));
                        buf.append(String.format("( %-3s -> ", sym));
                        for(String replace : mutationMap.get(kind).get(sym).opList){
                            buf.append(replace+" ");
                        }
                        buf.append(")\n");
                    }
                }
            }

            buf.append(indent+"} [");
            for(MutationOperator op:enabledOperators){
                buf.append("+"+op+", ");
            }
            for(MutationOperator op:disabledOperators){
                buf.append("-"+op+", ");
            }
            if((enabledOperators.size()+disabledOperators.size())>0){
                buf.delete(buf.length()-2, buf.length());
            }
            buf.append("]\n");

            for(String childName : children.keySet()){
                buf.append(getChild(childName).toString());
            }

            indent="";

            return buf.toString();
        }
    }

    /**
     * Recursively add type information to replacement lists of a given tree node
     * @param node The tree node that is to be transformed
     * @param tab The symbol table of which the type information are extracted
     * @return <code>true</code> if the transformation was successful
     */
    public static boolean attributeNode(TreeNode node, Symtab tab){
        // build data structures with type information and possible replacements
        Map<Type, LinkedList<OperatorSymbol>> opReplaceMap = new HashMap<Type, LinkedList<OperatorSymbol>>(64);
        Map<String, Set<Symbol>> binNameToSymbol = new HashMap<String, Set<Symbol>>(128);
        Map<String, Set<Symbol>> unrNameToSymbol = new HashMap<String, Set<Symbol>>(128);

        for (Symbol s : tab.predefClass.members().getElements()){
            if(! (s instanceof OperatorSymbol)) continue;

            if(opReplaceMap.containsKey(s.type)){
                opReplaceMap.get(s.type).addFirst((OperatorSymbol)s);
            }else{
                LinkedList<OperatorSymbol> l = new LinkedList<OperatorSymbol>();
                l.addFirst((OperatorSymbol)s);
                opReplaceMap.put(s.type, l);
            }
            int len = ((OperatorSymbol)s).getParameters().size();
            if(len==1){
                if(!unrNameToSymbol.containsKey(s.name.toString())){
                    unrNameToSymbol.put(s.name.toString(), new HashSet<Symbol>());
                }
                unrNameToSymbol.get(s.name.toString()).add(s);
                //System.out.println(s.name+" -> "+s.type.hashCode()+" -- "+unrOpMap.get(s.name.toString()));
            }else if(len==2){
                if(!binNameToSymbol.containsKey(s.name.toString())){
                    binNameToSymbol.put(s.name.toString(), new HashSet<Symbol>());
                }
                binNameToSymbol.get(s.name.toString()).add(s);
                //System.out.println(s.name+" -> "+s.type.hashCode()+" -- "+binNameToSymbol.get(s.name.toString()));
            }

            //System.out.println(s+" : "+s.type+"--"+((OperatorSymbol)s).getParameters());
        }


        Map<String, NodeEntry> binMap = node.getMutOperatorMap(MutationOperatorType.BIN);
        if(binMap!=null && binMap.size()>0){
            // Copy the original set since we will modify it on the fly
            Set<String> oldSet = new HashSet<String>(binMap.keySet());

            for(String str : oldSet){
                // Get old NodeEntry with the defined replacement list and remove this entry
                NodeEntry oldEntry = binMap.get(str);
                binMap.remove(str);

                // check if all specified operators are valid
                Set<String> validOps = new TreeSet<String>();
                for(Symbol sym : binNameToSymbol.get(str)){
                    for(Symbol s : opReplaceMap.get(sym.type)){
                        validOps.add(s.name.toString());
                    }
                }
                for(String checkOp : oldEntry.opList){
                    if(specialOperators.contains(checkOp)) continue;
                    if(!validOps.contains(checkOp)){
                        System.err.println("Error: Operator '"+checkOp+"' is not a valid replacement for the binary operator '"+str+"'");
                        return false;
                    }
                }

                // add all possible symbols
                for(Symbol sym : binNameToSymbol.get(str)){
                    List<String> l = new LinkedList<String>();
                    for(OperatorSymbol replace : opReplaceMap.get(sym.type)){
                        // Ignore the operator itself
                        if(replace.name.toString().equals(str)) continue;
                        // Ignore possible operators that have not been specified
                        if(!oldEntry.opList.contains(replace.name.toString())) continue;
                        l.add(replace.toString());
                    }
                    if(l.size()>=1) binMap.put(sym.toString(), new NodeEntry(l, oldEntry.replace));
                }
            }
        }
        Map<String, NodeEntry> unrMap = node.getMutOperatorMap(MutationOperatorType.UNR);
        if(unrMap!=null && unrMap.size()>0){
            // Copy the original set since we will modify it on the fly
            Set<String> oldSet = new HashSet<String>(unrMap.keySet());

            for(String str : oldSet){
                // Get old NodeEntry with the defined replacement list and remove this entry
                NodeEntry oldEntry = unrMap.get(str);
                unrMap.remove(str);

                // check if all specified operators are valid
                Set<String> validOps = new TreeSet<String>();
                for(Symbol sym : unrNameToSymbol.get(str)){
                    for(Symbol s : opReplaceMap.get(sym.type)){
                        validOps.add(s.name.toString());
                    }
                }
                for(String checkOp : oldEntry.opList){
                    if(!validOps.contains(checkOp)){
                        System.err.println("Error: Operator '"+checkOp+"' is not a valid replacement for the unary operator '"+str+"'");
                        return false;
                    }
                }

                // add all possible symbols
                for(Symbol sym : unrNameToSymbol.get(str)){
                    List<String> l = new LinkedList<String>();
                    for(OperatorSymbol replace : opReplaceMap.get(sym.type)){
                        // Ignore the operator itself
                        if(replace.name.toString().equals(str)) continue;
                        // Ignore possible operators that have not been specified
                        if(!oldEntry.opList.contains(replace.name.toString())) continue;
                        l.add(replace.toString());
                    }
                    if(l.size()>=1) unrMap.put(sym.toString(), new NodeEntry(l, oldEntry.replace));
                }
            }
        }

        for(TreeNode child : node.getChildren()){
            if(!attributeNode(child, tab)) return false;
        }

        return true;
    }
}
