package com.sun.tools.javac.mutation.context;

import com.sun.tools.javac.mutation.context.NodeContext.EdgeLabel;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCAnnotation;
import com.sun.tools.javac.tree.JCTree.JCArrayAccess;
import com.sun.tools.javac.tree.JCTree.JCArrayTypeTree;
import com.sun.tools.javac.tree.JCTree.JCAssert;
import com.sun.tools.javac.tree.JCTree.JCAssign;
import com.sun.tools.javac.tree.JCTree.JCAssignOp;
import com.sun.tools.javac.tree.JCTree.JCBinary;
import com.sun.tools.javac.tree.JCTree.JCBlock;
import com.sun.tools.javac.tree.JCTree.JCBreak;
import com.sun.tools.javac.tree.JCTree.JCCase;
import com.sun.tools.javac.tree.JCTree.JCCatch;
import com.sun.tools.javac.tree.JCTree.JCClassDecl;
import com.sun.tools.javac.tree.JCTree.JCCompilationUnit;
import com.sun.tools.javac.tree.JCTree.JCConditional;
import com.sun.tools.javac.tree.JCTree.JCContinue;
import com.sun.tools.javac.tree.JCTree.JCDoWhileLoop;
import com.sun.tools.javac.tree.JCTree.JCEnhancedForLoop;
import com.sun.tools.javac.tree.JCTree.JCErroneous;
import com.sun.tools.javac.tree.JCTree.JCExpressionStatement;
import com.sun.tools.javac.tree.JCTree.JCFieldAccess;
import com.sun.tools.javac.tree.JCTree.JCForLoop;
import com.sun.tools.javac.tree.JCTree.JCIdent;
import com.sun.tools.javac.tree.JCTree.JCIf;
import com.sun.tools.javac.tree.JCTree.JCImport;
import com.sun.tools.javac.tree.JCTree.JCInstanceOf;
import com.sun.tools.javac.tree.JCTree.JCLabeledStatement;
import com.sun.tools.javac.tree.JCTree.JCLiteral;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCMethodInvocation;
import com.sun.tools.javac.tree.JCTree.JCModifiers;
import com.sun.tools.javac.tree.JCTree.JCNewArray;
import com.sun.tools.javac.tree.JCTree.JCNewClass;
import com.sun.tools.javac.tree.JCTree.JCParens;
import com.sun.tools.javac.tree.JCTree.JCPrimitiveTypeTree;
import com.sun.tools.javac.tree.JCTree.JCReturn;
import com.sun.tools.javac.tree.JCTree.JCSkip;
import com.sun.tools.javac.tree.JCTree.JCSwitch;
import com.sun.tools.javac.tree.JCTree.JCSynchronized;
import com.sun.tools.javac.tree.JCTree.JCThrow;
import com.sun.tools.javac.tree.JCTree.JCTry;
import com.sun.tools.javac.tree.JCTree.JCTypeApply;
import com.sun.tools.javac.tree.JCTree.JCTypeCast;
import com.sun.tools.javac.tree.JCTree.JCTypeParameter;
import com.sun.tools.javac.tree.JCTree.JCTypeUnion;
import com.sun.tools.javac.tree.JCTree.JCUnary;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;
import com.sun.tools.javac.tree.JCTree.JCWhileLoop;
import com.sun.tools.javac.tree.JCTree.JCWildcard;
import com.sun.tools.javac.tree.JCTree.LetExpr;
import com.sun.tools.javac.tree.JCTree.TypeBoundKind;
import com.sun.tools.javac.tree.TreeInfo;

/**
 * This visitor determines and returns the program context associated with
 * a given tree node.
 *
 */
public class NodeContextVisitor extends JCTree.Visitor {
    private NodeContext context;
    private JCTree child;

    public NodeContext getContext(JCTree tree, JCTree child) {
        this.child = child;
        this.context = null;
        tree.accept(this);
        return context;
    }

    public void visitTopLevel(JCCompilationUnit tree) {
        context = new NodeContext(tree, null);
    }

    public void visitImport(JCImport tree) {
        context = new NodeContext(tree, null);
    }

    public void visitClassDef(JCClassDecl tree) {
        context = new NodeContext(tree, null);
    }

    public void visitMethodDef(JCMethodDecl tree) {
        context = new NodeContext(tree, null);
    }

    public void visitVarDef(JCVariableDecl tree) {
        context = new NodeContext(tree, null);
    }

    public void visitSkip(JCSkip tree) {
        context = new NodeContext(tree, null);
    }

    public void visitBlock(JCBlock tree) {
        context = new NodeContext(tree, null);
    }

    public void visitDoLoop(JCDoWhileLoop tree) {
        if (tree.body == child) {
            context = new NodeContext(tree, EdgeLabel.BODY);
        } else if (tree.cond == child) {
            context = new NodeContext(tree, EdgeLabel.COND);
        } else {
            context = new NodeContext(tree, null);
        }
        setChildContext(tree.cond);
    }

    public void visitWhileLoop(JCWhileLoop tree) {
        if (tree.body == child) {
            context = new NodeContext(tree, EdgeLabel.BODY);
        } else if (tree.cond == child) {
            context = new NodeContext(tree, EdgeLabel.COND);
        } else {
            context = new NodeContext(tree, null);
        }
        setChildContext(tree.cond);
    }

    public void visitForLoop(JCForLoop tree) {
        if (tree.body == child) {
            context = new NodeContext(tree, EdgeLabel.BODY);
        } else if (tree.cond == child) {
            context = new NodeContext(tree, EdgeLabel.COND);
        } else if (tree.init != null && tree.init.contains(child)) {
            context = new NodeContext(tree, EdgeLabel.INIT);
        } else if (tree.step != null && tree.step.contains(child)) {
            context = new NodeContext(tree, EdgeLabel.STEP);
        } else {
            context = new NodeContext(tree, null);
        }
        setChildContext(tree.cond);
    }

    public void visitForeachLoop(JCEnhancedForLoop tree) {
        if (tree.body == child) {
            context = new NodeContext(tree, EdgeLabel.BODY);
        } else if (tree.expr == child) {
            context = new NodeContext(tree, EdgeLabel.EXPR);
        } else if (tree.var == child) {
            context = new NodeContext(tree, EdgeLabel.VAR);
        } else {
            context = new NodeContext(tree, null);
        }
        setChildContext(tree.expr);
    }

    public void visitLabelled(JCLabeledStatement tree) {
        context = new NodeContext(tree, null);
    }

    public void visitSwitch(JCSwitch tree) {
        context = new NodeContext(tree, null);
    }

    public void visitCase(JCCase tree) {
        context = new NodeContext(tree, null);
    }

    public void visitSynchronized(JCSynchronized tree) {
        context = new NodeContext(tree, null);
    }

    public void visitTry(JCTry tree) {
        context = new NodeContext(tree, null);
    }

    public void visitCatch(JCCatch tree) {
        context = new NodeContext(tree, null);
    }

    public void visitConditional(JCConditional tree) {
        if (tree.cond == child) {
            context = new NodeContext(tree, EdgeLabel.COND);
        } else if (tree.truepart == child) {
            context = new NodeContext(tree, EdgeLabel.TRUEPART);
        } else if (tree.falsepart == child) {
            context = new NodeContext(tree, EdgeLabel.FALSEPART);
        } else {
            context = new NodeContext(tree, null);
        }
        setChildContext(tree.cond, tree.truepart, tree.falsepart);
    }

    public void visitIf(JCIf tree) {
        if (tree.cond == child) {
            context = new NodeContext(tree, EdgeLabel.COND);
        } else if (tree.thenpart == child) {
            context = new NodeContext(tree, EdgeLabel.THENPART);
        } else if (tree.elsepart == child) {
            context = new NodeContext(tree, EdgeLabel.ELSEPART);
        } else {
            context = new NodeContext(tree, null);
        }
        setChildContext(tree.cond);
    }

    public void visitExec(JCExpressionStatement tree) {
        context = new NodeContext(tree, null);
    }

    public void visitBreak(JCBreak tree) {
        context = new NodeContext(tree, null);
    }

    public void visitContinue(JCContinue tree) {
        context = new NodeContext(tree, null);
    }

    public void visitReturn(JCReturn tree) {
        context = new NodeContext(tree, null);
        setChildContext(tree.expr);
    }

    public void visitThrow(JCThrow tree) {
        context = new NodeContext(tree, null);
    }

    public void visitAssert(JCAssert tree) {
        context = new NodeContext(tree, null);
    }

    public void visitApply(JCMethodInvocation tree) {
        context = new NodeContext(tree, null);
    }

    public void visitNewClass(JCNewClass tree) {
        context = new NodeContext(tree, null);
    }

    public void visitNewArray(JCNewArray tree) {
        if (tree.dims != null && tree.dims.contains(child)) {
            context = new NodeContext(tree, EdgeLabel.DIMS);
        } else if (tree.elems != null && tree.elems.contains(child)) {
            context = new NodeContext(tree, EdgeLabel.ELEMS);
        } else {
            context = new NodeContext(tree, null);
        }
    }

    public void visitParens(JCParens tree) {
        context = new NodeContext(tree, null);
    }

    public void visitAssign(JCAssign tree) {
        if (tree.lhs == child) {
            context = new NodeContext(tree, EdgeLabel.LHS);
        } else if (tree.rhs == child) {
            context = new NodeContext(tree, EdgeLabel.RHS);
        } else {
            context = new NodeContext(tree, null);
        }
        setChildContext(tree.lhs, tree.rhs);
    }

    public void visitAssignop(JCAssignOp tree) {
        if (tree.lhs == child) {
            context = new NodeContext(tree, EdgeLabel.LHS);
        } else if (tree.rhs == child) {
            context = new NodeContext(tree, EdgeLabel.RHS);
        } else {
            context = new NodeContext(tree, null);
        }
        setChildContext(tree.lhs, tree.rhs);
    }

    public void visitUnary(JCUnary tree) {
        context = new NodeContext(tree, null);
        setChildContext(tree.arg);

    }

    public void visitBinary(JCBinary tree) {
        if (tree.lhs == child) {
            context = new NodeContext(tree, EdgeLabel.LHS);
        } else if (tree.rhs == child) {
            context = new NodeContext(tree, EdgeLabel.RHS);
        } else {
            context = new NodeContext(tree, null);
        }
        setChildContext(tree.lhs, tree.rhs);
    }

    public void visitTypeCast(JCTypeCast tree) {
        context = new NodeContext(tree, null);
    }

    public void visitTypeTest(JCInstanceOf tree) {
        context = new NodeContext(tree, null);
    }

    public void visitIndexed(JCArrayAccess tree) {
        if (tree.indexed == child) {
            context = new NodeContext(tree, EdgeLabel.INDEXED);
        } else if (tree.index == child) {
            context = new NodeContext(tree, EdgeLabel.INDEX);
        } else {
            context = new NodeContext(tree, null);
        }
        setChildContext(tree.indexed, tree.index);
    }

    public void visitSelect(JCFieldAccess tree) {
        context = new NodeContext(tree, null);
    }

    public void visitIdent(JCIdent tree) {
        context = new NodeContext(tree, null);
    }

    public void visitLiteral(JCLiteral tree) {
        context = new NodeContext(tree, null);
    }

    public void visitTypeIdent(JCPrimitiveTypeTree tree) {
        context = new NodeContext(tree, null);
    }

    public void visitTypeArray(JCArrayTypeTree tree) {
        context = new NodeContext(tree, null);
    }

    public void visitTypeApply(JCTypeApply tree) {
        context = new NodeContext(tree, null);
    }

    public void visitTypeUnion(JCTypeUnion tree) {
        context = new NodeContext(tree, null);
    }

    public void visitTypeParameter(JCTypeParameter tree) {
        context = new NodeContext(tree, null);
    }

    @Override
    public void visitWildcard(JCWildcard tree) {
        context = new NodeContext(tree, null);
    }

    @Override
    public void visitTypeBoundKind(TypeBoundKind that) {
        context = new NodeContext(that, null);
    }

    public void visitModifiers(JCModifiers tree) {
        context = new NodeContext(tree, null);
    }

    public void visitAnnotation(JCAnnotation tree) {
        context = new NodeContext(tree, null);
    }

    public void visitErroneous(JCErroneous tree) {
        context = new NodeContext(tree, null);
    }

    public void visitLetExpr(LetExpr tree) {
        context = new NodeContext(tree, null);
    }

    private void setChildContext(JCTree ... children) {
        context.setHasLiteralChild(containsLiteral(children));
        context.setHasVariableChild(containsVariable(children));
        context.setHasOperatorChild(containsOperator(children));
    }

    /**
     * Returns true if any of the children is a literal
     */
    private boolean containsLiteral(JCTree ... children) {
        if (children == null) {
            return false;
        };
        for (JCTree child : children) {
            if (child == null) {
                return false;
            }
            JCTree tree = TreeInfo.skipParens(child);
            switch(tree.getKind()) {
                case INT_LITERAL:
                case FLOAT_LITERAL:
                case LONG_LITERAL:
                case DOUBLE_LITERAL:
                case CHAR_LITERAL:
                case STRING_LITERAL:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * Returns true if any of the children is an identifier or a member select
     */
    private boolean containsVariable(JCTree ... children) {
        if (children == null) {
            return false;
        };
        for (JCTree child : children) {
            if (child == null) {
                return false;
            }
            JCTree tree = TreeInfo.skipParens(child);
            switch(tree.getKind()) {
                case MEMBER_SELECT:
                case IDENTIFIER:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * Returns true if any of the children is a binary or unary operator
     */
    private boolean containsOperator(JCTree ... children) {
        if (children == null) {
            return false;
        };
        for (JCTree child : children) {
            if (child == null) {
                return false;
            }
            JCTree tree = TreeInfo.skipParens(child);
            if (tree instanceof JCBinary || tree instanceof JCUnary) {
                return true;
            }
        }
        return false;
    }
}
