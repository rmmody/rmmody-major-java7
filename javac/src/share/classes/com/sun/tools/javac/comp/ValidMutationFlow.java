package com.sun.tools.javac.comp;

import static com.sun.tools.javac.code.Flags.BLOCK;
import static com.sun.tools.javac.code.Flags.FINAL;
import static com.sun.tools.javac.code.Flags.PARAMETER;
import static com.sun.tools.javac.code.Flags.STATIC;
import static com.sun.tools.javac.code.Flags.UNION;
import static com.sun.tools.javac.code.Kinds.TYP;
import static com.sun.tools.javac.code.TypeTags.VOID;

import java.util.ArrayList;

import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.code.Lint;
import com.sun.tools.javac.code.Symbol.VarSymbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTags;
import com.sun.tools.javac.comp.Flow.PendingExit;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCCatch;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCTry;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;
import com.sun.tools.javac.tree.TreeInfo;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Assert;
import com.sun.tools.javac.util.Bits;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.ListBuffer;

/**
 *
 * @author rjust
 *
 * TODO Use custom Log class to encounter all flow violating cases
 *      rather than overriding individual methods of the Flow class.
 */
public class ValidMutationFlow extends Flow {

    public static final int FLOW_VIOLATION=1;
    public static final int UNREACHABLE_STMT=2;
    public static final int NONTERM_INIT=4;

    private static int problems=0;

    /**
     * Check whether the method declaration contains any flow violations
     *
     * @param context The current context, used to save and restore the original flow checker
     * @param tree the tree to check for uninitialized local variables
     * @return Returns <code>true</code> if the tree <code>tree</code> has flow violations, <code>false</code> otherwise
     */
    public static int checkFlowViolations(Context context, Env<AttrContext> env, TreeMaker make){
        Flow oldFlow = context.get(flowKey);
        context.put(flowKey, (Flow)null);

        ValidMutationFlow flow = new ValidMutationFlow(context);
        // reset problem bit mask
        problems=0;
        try{

            flow.analyzeTree(env, make);
        }catch(FlowViolationException e){
            problems |= FLOW_VIOLATION;
        }

        context.put(flowKey, (Flow)null);
        context.put(flowKey, oldFlow);

        // reset var addresses
        for(VarSymbol sym : flow.trackedVars){
            sym.adr=-1;
        }

        return problems;
    }

    private static boolean addInit=false;
    private static JCTree toplevel;

    /**
     * Add initializers to flow violating unititialized local variables
     *
     * @param context The current context, used to save and restore the original flow checker
     * @param tree the tree to check for uninitialized local variables
     * @return Returns <code>true</code> if the tree <code>tree</code> has flow violations, <code>false</code> otherwise
     */
    public static void fixFlowViolations(Context context, Env<AttrContext> env, TreeMaker make){
        addInit=true;
        toplevel=env.tree;
        int problems = checkFlowViolations(context, env, make);
        assert problems==0 : "Unexpected flow violation in mutated AST";
        addInit=false;
        toplevel=null;
    }


    private ValidMutationFlow(Context c){
        super(c);
        tab = Symtab.instance(c);
        chk = Check.instance(c);
        lint= Lint.instance(c);
    }

    /*
     * Keep track of all analyzed vars -> reset the address after the flow check
     */
    private java.util.List<VarSymbol> trackedVars = new ArrayList<VarSymbol>(64);
    private TreeMaker make;
    private Symtab tab;
    private final Check chk;
    private Lint lint;

    @Override
    public void analyzeTree(Env<AttrContext> env, TreeMaker make) {
        this.make=make;
        super.analyzeTree(env, make);
    }

    @Override
    public void visitMethodDef(JCMethodDecl tree) {
        if (tree.body == null) return;

        List<Type> caughtPrev = caught;
        List<Type> mthrown = tree.sym.type.getThrownTypes();
        Bits initsPrev = inits.dup();
        Bits uninitsPrev = uninits.dup();
        int nextadrPrev = nextadr;
        int firstadrPrev = firstadr;
        Lint lintPrev = lint;

        lint = lint.augment(tree.sym.attributes_field);

        Assert.check(pendingExits.isEmpty());

        try {
            boolean isInitialConstructor =
                TreeInfo.isInitialConstructor(tree);

            if (!isInitialConstructor)
                firstadr = nextadr;
            for (List<JCVariableDecl> l = tree.params; l.nonEmpty(); l = l.tail) {
                JCVariableDecl def = l.head;
                scan(def);
                inits.incl(def.sym.adr);
                uninits.excl(def.sym.adr);
            }
            if (isInitialConstructor)
                caught = chk.union(caught, mthrown);
            else if ((tree.sym.flags() & (BLOCK | STATIC)) != BLOCK)
                caught = mthrown;
            // else we are in an instance initializer block;
            // leave caught unchanged.

            alive = true;
            scanStat(tree.body);

            if (alive && tree.sym.type.getReturnType().tag != VOID) {
                //"missing.ret.stmt"
                throw new FlowViolationException();
            }
            if (isInitialConstructor) {
                for (int i = firstadr; i < nextadr; i++)
                    if (vars[i].owner == classDef.sym)
                        checkInit(TreeInfo.diagEndPos(tree.body), vars[i]);
            }
            List<PendingExit> exits = pendingExits.toList();
            pendingExits = new ListBuffer<PendingExit>();
            while (exits.nonEmpty()) {
                PendingExit exit = exits.head;
                exits = exits.tail;
                if (exit.thrown == null) {
                    if(exit.tree.getTag() != JCTree.RETURN) {
                        //TODO: This shouldn't happen -> Check the root cause rather than excluding the mutants
                        throw new FlowViolationException();
                    }
                    Assert.check(exit.tree.getTag() == JCTree.RETURN);
                    if (isInitialConstructor) {
                        inits = exit.inits;
                        for (int i = firstadr; i < nextadr; i++)
                            checkInit(exit.tree.pos(), vars[i]);
                    }
                } else {
                    // uncaught throws will be reported later
                    pendingExits.append(exit);
                }
            }
        } finally {
            inits = initsPrev;
            uninits = uninitsPrev;
            nextadr = nextadrPrev;
            firstadr = firstadrPrev;
            caught = caughtPrev;
            lint = lintPrev;
        }
    }

    @Override
    void newVar(VarSymbol sym) {
        trackedVars.add(sym);
        super.newVar(sym);
    }

    @Override
    void checkInit(DiagnosticPosition pos, VarSymbol sym) {
        if ((sym.adr >= firstadr || sym.owner.kind != TYP) &&
                trackable(sym) &&
                !inits.isMember(sym.adr)) {
                    if(addInit){
                        JCVariableDecl varDecl;
                        addDefaultInit(varDecl=(JCVariableDecl)TreeInfo.declarationFor(sym, toplevel));
                        return;
                    }
                    // We need to rollback this mutant, so there is no need to analyze it further!
                    throw new FlowViolationException();
            }
    }

    /** Record an initialization of a trackable variable.
     */
    void letInit(DiagnosticPosition pos, VarSymbol sym) {
        if (sym.adr >= firstadr && trackable(sym)) {
            if ((sym.flags() & FINAL) != 0) {
                if ((sym.flags() & PARAMETER) != 0) {
                    if ((sym.flags() & UNION) != 0) { //multi-catch parameter
                        // We need to rollback this mutant, so there is no need to analyze it further!
                        throw new FlowViolationException();
                    }
                    else {
                        // We need to rollback this mutant, so there is no need to analyze it further!
                        throw new FlowViolationException();
                    }
                } else if (!uninits.isMember(sym.adr)) {
                    // We need to rollback this mutant, so there is no need to analyze it further!
                    throw new FlowViolationException();
                } else if (!inits.isMember(sym.adr)) {
                    // reachable assignment
                    uninits.excl(sym.adr);
                    uninitsTry.excl(sym.adr);
                } else {
                    //log.rawWarning(pos, "unreachable assignment");//DEBUG
                    uninits.excl(sym.adr);
                }
            }
            inits.incl(sym.adr);
        } else if ((sym.flags() & FINAL) != 0) {
            // We need to rollback this mutant, so there is no need to analyze it further!
            throw new FlowViolationException();
        }
    }

    @Override
    void scanStat(JCTree tree) {
        if (!alive && tree != null) {
            // we found an unreachable statement -> mutant will nevertheless compile!
            problems |= UNREACHABLE_STMT;
            if (tree.getTag() != JCTree.SKIP) alive = true;
        }
        scan(tree);
    }


    @Override
    void scanDef(JCTree tree) {
        scanStat(tree);
        if (tree != null && tree.getTag() == JCTree.BLOCK && !alive) {
            // problem caused by an unreachable statement -> indicate that the initializer is affected
            // the mutant will nevertheless compile!
            problems |= NONTERM_INIT;
        }
    }

    @Override
    public void visitTry(JCTry tree) {
        List<Type> caughtPrev = caught;
        List<Type> thrownPrev = thrown;
        thrown = List.nil();
        for (List<JCCatch> l = tree.catchers; l.nonEmpty(); l = l.tail)
            caught = chk.incl(l.head.param.type, caught);
        Bits uninitsTryPrev = uninitsTry;
        ListBuffer<PendingExit> prevPendingExits = pendingExits;
        pendingExits = new ListBuffer<PendingExit>();
        Bits initsTry = inits.dup();
        uninitsTry = uninits.dup();
        scanStat(tree.body);
        List<Type> thrownInTry = thrown;
        thrown = thrownPrev;
        caught = caughtPrev;
        boolean aliveEnd = alive;
        uninitsTry.andSet(uninits);
        Bits initsEnd = inits;
        Bits uninitsEnd = uninits;
        int nextadrCatch = nextadr;

        List<Type> caughtInTry = List.nil();
        for (List<JCCatch> l = tree.catchers; l.nonEmpty(); l = l.tail) {
            alive = true;
            JCVariableDecl param = l.head.param;
            Type exc = param.type;
            if (chk.subset(exc, caughtInTry)) {
                throw new FlowViolationException();
            } else if (!chk.isUnchecked(l.head.pos(), exc) &&
                       exc.tsym != tab.throwableType.tsym &&
                       exc.tsym != tab.exceptionType.tsym &&
                       !chk.intersects(exc, thrownInTry)) {
                throw new FlowViolationException();
            }
            caughtInTry = chk.incl(exc, caughtInTry);
            inits = initsTry.dup();
            uninits = uninitsTry.dup();
            scan(param);
            inits.incl(param.sym.adr);
            uninits.excl(param.sym.adr);
            scanStat(l.head.body);
            initsEnd.andSet(inits);
            uninitsEnd.andSet(uninits);
            nextadr = nextadrCatch;
            aliveEnd |= alive;
        }
        if (tree.finalizer != null) {
            List<Type> savedThrown = thrown;
            thrown = List.nil();
            inits = initsTry.dup();
            uninits = uninitsTry.dup();
            ListBuffer<PendingExit> exits = pendingExits;
            pendingExits = prevPendingExits;
            alive = true;
            scanStat(tree.finalizer);
            if (!alive) {
                // discard exits and exceptions from try and finally
                thrown = chk.union(thrown, thrownPrev);
                if (!loopPassTwo &&
                    lint.isEnabled(Lint.LintCategory.FINALLY)) {
                    throw new FlowViolationException();
                }
            } else {
                thrown = chk.union(thrown, chk.diff(thrownInTry, caughtInTry));
                thrown = chk.union(thrown, savedThrown);
                uninits.andSet(uninitsEnd);
                // FIX: this doesn't preserve source order of exits in catch
                // versus finally!
                while (exits.nonEmpty()) {
                    PendingExit exit = exits.next();
                    if (exit.inits != null) {
                        exit.inits.orSet(inits);
                        exit.uninits.andSet(uninits);
                    }
                    pendingExits.append(exit);
                }
                inits.orSet(initsEnd);
                alive = aliveEnd;
            }
        } else {
            thrown = chk.union(thrown, chk.diff(thrownInTry, caughtInTry));
            inits = initsEnd;
            uninits = uninitsEnd;
            alive = aliveEnd;
            ListBuffer<PendingExit> exits = pendingExits;
            pendingExits = prevPendingExits;
            while (exits.nonEmpty()) pendingExits.append(exits.next());
        }
        uninitsTry.andSet(uninitsTryPrev).andSet(uninits);
    }


    private void addDefaultInit(JCVariableDecl tree){
        if((tree.mods.flags & Flags.STATIC) == 0){
            tree.sym.flags_field &= ~Flags.FINAL;
            tree.mods.flags &= ~Flags.FINAL;
        }

        switch(tree.type.tag){
            case TypeTags.BOOLEAN:
            case TypeTags.CHAR:
            case TypeTags.INT:
                tree.init=make.Literal(0).setPos(tree.pos).setType(tree.type.constType(0));
                break;
            case TypeTags.BYTE:
                tree.init=make.Literal((byte)0).setPos(tree.pos);
                break;
            case TypeTags.SHORT:
                tree.init=make.Literal((short)0).setPos(tree.pos);
                break;
            case TypeTags.LONG:
                tree.init=make.Literal((long)0).setPos(tree.pos);
                break;

            case TypeTags.FLOAT:
                tree.init=make.Literal((float)0.).setPos(tree.pos);
                break;
            case TypeTags.DOUBLE:
                tree.init=make.Literal(0.).setPos(tree.pos);
                break;
            default:
                tree.init=make.Literal(TypeTags.BOT, null).setPos(tree.pos).setType(tab.botType);
                break;
        }
    }

    @Override
    void errorUncaught() {
        for (PendingExit exit = pendingExits.next();
             exit != null;
             exit = pendingExits.next()) {
            if (classDef != null &&
                classDef.pos == exit.tree.pos) {
                    //"unreported.exception.default.constructor",
                    // We need to rollback this mutant, so there is no need to analyze it further!
                    throw new FlowViolationException();
            } else if (exit.tree.getTag() == JCTree.VARDEF &&
                    ((JCVariableDecl)exit.tree).sym.isResourceVariable()) {
                // "unreported.exception.implicit.close"
                // We need to rollback this mutant, so there is no need to analyze it further!
                throw new FlowViolationException();
            } else {
                //"unreported.exception.need.to.catch.or.throw"
                // We need to rollback this mutant, so there is no need to analyze it further!
                throw new FlowViolationException();
            }
        }
    }

    /*
     * Exception to indicate a flow violation and interrupt the flow checker
     */
    private static class FlowViolationException extends RuntimeException{
        public static final long serialVersionUID = 1L;
    }
}
