Major mutation framework
------------------------

The Major mutation framework consists of the following components:

| Component | Description                                                                   |
|-----------|-------------------------------------------------------------------------------|
| javac     | The mutator (AST rewriter), which is integrated into the Java compiler        |
| mmlc      | The DSL for configuring the mutator at compile time                           |
| config    | The run-time library that enables mutants and collects coverage information   |
| ant       | The mutation analysis back-end, which is integrated into Apache Ant           |
| eclipse   | The Eclipse plugin (beta)                                                     |

# How to build Major's components

## javac
`ant -f make/build.xml build-javac`

## mmlc
`ant dist`

## config
`ant dist`

## ant
`ant jars`

## manual
`make`

# How to deploy Major's Eclipse plugin

1. Extract the eclipse plugin directory into your Eclipse workspace. You may rename this directory if you wish.
2. Import the directory as a project into Eclipse.
3. In the Package Explorer, right click on the project to bring up the context menu.
4. In the context menu, select "Export".
5. Under "Plug-in Development", select "Deployable plug-ins and fragments". If the "Plug-in Development" folder does not show up, you can freely download and install the Eclipse Plugin Development Tools.
6. Under the "Destination" tab, select "Directory".
7. For the "Directory", select the "dropins" folder under your eclipse directory.
8. Click "Finish".
9. Restart Eclipse and a "Mutate" button should appear on the workbench. See README in the eclipse plugin directory for instructions on using the plugin.

# How to build a release
```
./release/release.sh <version>
```
This script builds all components and the manual, and bundles everything into a
releasable archive. As a sanity check, the provided argument (<version>) must
match the version (as listed in major.properties) of the working directory.
