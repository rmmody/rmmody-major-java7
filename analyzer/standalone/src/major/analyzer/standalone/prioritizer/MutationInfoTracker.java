package major.analyzer.standalone.prioritizer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import major.analyzer.standalone.data.MutationInfo;

public class MutationInfoTracker {
		
	// track the list of mutants covered per Test Class to optimise on
	// running time during Mutation Analysis
	public static Map<String, Set<Integer>> trackCoveredMutantsPerClass(Map<String, MutationInfo> prepPassInfo) {

		Map<String, Set<Integer>> mutationInfoClassMap = new HashMap<>();

		for (Map.Entry<String, MutationInfo> entry : prepPassInfo.entrySet()) {
			
			//Retrieve the class containing the test case entry 
			String className = entry.getValue().getAssociatedClassName();
			//Retrieve ids of mutants killed by test case
			List<Integer> currMutantIDs = entry.getValue().getMutantIds();
			//Store id of mutant for it to correspond to the class name
			if (mutationInfoClassMap.containsKey(className)) {
				Set<Integer> idSet = mutationInfoClassMap.get(className);
				idSet.addAll(currMutantIDs);
			} else {
				Set<Integer> idSet = new HashSet<>();
				idSet.addAll(currMutantIDs);
				mutationInfoClassMap.put(className, idSet);
			}
		}

		return mutationInfoClassMap;
	}

}
