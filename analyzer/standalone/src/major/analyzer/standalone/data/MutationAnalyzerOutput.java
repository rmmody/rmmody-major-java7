package major.analyzer.standalone.data;

import java.util.ArrayList;
import java.util.List;

public class MutationAnalyzerOutput {

	/**
	 * Stores : Mutation Kill Score, Summary and Note on Un-Killed Mutants. Also
	 * stores run time of the code.
	 */

	private int totalMutantCount;
	private int mutantsKilled;
	private int mutantsKilledTimeout;
	private List<Integer> mutantIdsNotKilled;
	private long programRunTime;

	public MutationAnalyzerOutput() {
		mutantIdsNotKilled = new ArrayList<>();
	}

	//Retrieve the count of mutants added to the code files
	public int getTotalMutantCount() {
		return totalMutantCount;
	}

	//Store the count of mutants added to the code files
	public void setTotalMutantCount(int totalMutantCount) {
		this.totalMutantCount = totalMutantCount;
	}

	//Retrieve the count of mutants killed by the analyzer
	public int getMutantsKilled() {
		return mutantsKilled;
	}
	//Store the count of mutants killed by the analyzer
	public void setMutantsKilled(int mutantsKilled) {
		this.mutantsKilled = mutantsKilled;
	}

	//Retrieve the count of mutants killed by timeout
	public int getMutantsKilledTimeout() {
		return mutantsKilledTimeout;
	}

	//Store the count of mutants killed by timeout
	public void setMutantsKilledTimeout(int mutantsKilledTimeout) {
		this.mutantsKilledTimeout = mutantsKilledTimeout;
	}

	//Retrieve the ids of mutants that have been not killed
	public List<Integer> getMutantIdsNotKilled() {
		return mutantIdsNotKilled;
	}

	//Store the ids of mutants that have been not killed
	public void setMutantIdsNotKilled(List<Integer> mutantIdsNotKilled) {
		this.mutantIdsNotKilled = mutantIdsNotKilled;
	}

	//Retrieve the total run time of the code
	public long getProgramRunTime() {
		return programRunTime;
	}

	//Store the total run time of the code
	public void setProgramRunTime(long programRunTime) {
		this.programRunTime = programRunTime;
	}

}
