package major.analyzer.standalone.output;

import major.analyzer.standalone.data.MutationAnalyzerOutput;

public interface OutputView {
	
	public void displayComponent(MutationAnalyzerOutput output);

}
