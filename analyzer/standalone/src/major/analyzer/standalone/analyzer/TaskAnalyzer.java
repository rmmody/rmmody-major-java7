package major.analyzer.standalone.analyzer;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import org.junit.runner.JUnitCore;
import org.junit.runner.Request;
import org.junit.runner.Result;

import major.mutation.Config;

@SuppressWarnings("rawtypes")
public class TaskAnalyzer implements Callable<Integer> {

	private List<Class> testClasses;
	private int mutantId;
	private Map<String, List<String>> prioritizedTestSuiteInfo;
	private Map<String, Set<Integer>> mutationInfoPerClass;

	public TaskAnalyzer(List<Class> testClasses, int mutantId, Map<String, List<String>> prioritizedTestSuiteInfo,
			Map<String, Set<Integer>> mutationInfoPerClass) {
		this.testClasses = testClasses;
		this.mutantId = mutantId;
		this.prioritizedTestSuiteInfo = prioritizedTestSuiteInfo;
		this.mutationInfoPerClass = mutationInfoPerClass;
	}

	@Override
	public Integer call() throws Exception {
		int status = 0;
		try {
			// Set Config in "major" tool to the current mutant ID
			Config.__M_NO = mutantId;
			for (Class eachClass : testClasses) {
				// iterate over ordered list of test cases in that class
				// run only those tests that cover the current mutant id
				if (!this.mutationInfoPerClass.get(eachClass.getName()).contains(mutantId)) {
					continue;
				}
				List<String> methodNames = prioritizedTestSuiteInfo.get(eachClass.getName());
				for (int i = 0; i < methodNames.size(); i++) {
					Request runRequest = Request.method(eachClass, methodNames.get(i));
					//Running the JUnit tests
					Result runResult = new JUnitCore().run(runRequest);
					if (runResult.getFailureCount() != 0) {
						// Mutant Killed - Test Run Failed - Set Status - Break
						// Loop
						status = 1;
						break;
					}
				}
				// Break loop once mutant has been killed - Assumes one mutant
				// per mutant-ID only
				if (status == 1) {
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		// reset config
		Config.reset();
		return new Integer(status);
	}

	//Retirve the test classes loaded from the jar files
	public List<Class> getTestClasses() {
		return testClasses;
	}

	//Store the test classes' names that are in the jar files
	public void setTestClasses(List<Class> testClasses) {
		this.testClasses = testClasses;
	}

	//Retrieve id of a particular mutant
	public int getMutantId() {
		return mutantId;
	}

	//Store id of a particular mutant
	public void setMutantId(int mutantId) {
		this.mutantId = mutantId;
	}

	//Retrieve mutant related information for each test class
	public Map<String, Set<Integer>> getMutationInfoPerClass() {
		return mutationInfoPerClass;
	}

	//Store mutant related information for each test class
	public void setMutationInfoPerClass(Map<String, Set<Integer>> mutationInfoPerClass) {
		this.mutationInfoPerClass = mutationInfoPerClass;
	}

}
