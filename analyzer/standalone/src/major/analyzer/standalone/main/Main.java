package major.analyzer.standalone.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JFrame;

import major.analyzer.standalone.data.MutationAnalyzerOutput;
import major.analyzer.standalone.data.MutationInfo;
import major.analyzer.standalone.analyzer.MutationAnalyzer;
import major.analyzer.standalone.output.CompositeView;
import major.analyzer.standalone.output.MutationKillSummaryView;
import major.analyzer.standalone.output.MutationScoreView;
import major.analyzer.standalone.output.RunTimeInfoView;
import major.analyzer.standalone.output.UnKilledMutantInfoView;
import major.analyzer.standalone.prePass.PrePassAnalyzer;
import major.analyzer.standalone.prioritizer.MutationInfoTracker;
import major.analyzer.standalone.prioritizer.TestSuitePrioritizer;
import major.analyzer.standalone.util.TestCaseClassLoader;

@SuppressWarnings("rawtypes")
public class Main {

	public static void main(String[] args) {

		// requires path location to (1) compiled jar file for mutated source code (2) compiled jar file for test classes
		// (3) mutants.log file
		if (args.length != 3) {
			System.out.println(
					"Please correctly enter : (1) Path to Compile Source Jar File ; (2) Path to Test Jar File ;(3) Path to Mutant log file");
			System.out.println("Exiting!!!");
			return;
		}

		System.out.println("-----Starting Mutation Analysis-----");
		//Store the path to the jar files and the log file
		String classjarLocation = args[0];
		String testjarLocation = args[1];
		String mutationLogFileLocation = args[2];
		


		// track the running time of the program
		long startOfCode = System.currentTimeMillis();

		// STEP 0 : Load the jar files using the provided path and extract all test classes from the provided Jar and
		// load them.
		List<Class> testClassList = TestCaseClassLoader.loadTestClasses(classjarLocation ,testjarLocation);

		// STEP 1 : Pre-pass phase to analyze coverage and run-time information
		// for each test-case.
		// Return a map of full test case names (className+FuncName) v/s an
		// object of MutationInfo containing the mutant ids it covers and the
		// run time of the corresponding test case.
		System.out.println("\n STEP 1 : Pre-Pass (Collecting Mutation coverage and run-time info for each test case)");

		PrePassAnalyzer ppa = new PrePassAnalyzer();
		ppa.setLoadedTestClasses(testClassList);
		Map<String, MutationInfo> prepPassInfo = ppa.performPrePassAnalysis();

		// STEP 2 : Use info populated in the map from STEP 1 to prioritize test
		// suite for running actual Mutation analyzer.
		// Returns a map of classNames v/s the respective order of each test
		// case for execution in that class
		// Also maps each Test class to the mutant Ids it covers.
		// To be used later for optimisation.
		System.out.println("\n STEP 2 : Performing Test Suite Prioritization");

		Map<String, List<String>> prioritizedTests = TestSuitePrioritizer.prioritize(prepPassInfo);
		Map<String, Set<Integer>> coveredMutantsByClass = MutationInfoTracker.trackCoveredMutantsPerClass(prepPassInfo);

		// STEP 3 : A mutationAnalyzer object is initialized and all the information collected from the previous steps are added to it
		//Conducting Strong Mutation Analysis by running each test
		// case on each mutant to track mutation kill information
		// Returns an object with Mutation Kill Info populated.
		System.out.println("\n STEP 3 : Performing Strong Mutation Analysis");

		MutationAnalyzer mutationAnalyzer = new MutationAnalyzer();

		mutationAnalyzer.setLoadedTestClasses(testClassList);
		mutationAnalyzer.setMutationLogFileLocation(mutationLogFileLocation);
		mutationAnalyzer.setPrioritizedTestSuiteInfo(prioritizedTests);
		mutationAnalyzer.setMutationInfoPerClass(coveredMutantsByClass);

		// store output (Mutation Kill Information, Kill Summary and Code
		// RunTime)
		MutationAnalyzerOutput output = null;
		try {
			output = mutationAnalyzer.analyzeMutants();
		} catch (Exception e) {
			e.printStackTrace();
		}

		//End of tracking the running time of the program
		long endOfCode = System.currentTimeMillis();
		output.setProgramRunTime(endOfCode - startOfCode);

		// Display the desired components of the output.
		JFrame mainFrame = new JFrame("Major Mutation Framework : Mutation Analyser");
		mainFrame.setLayout(new FlowLayout());

		// Initialize the main composite view and add all the desired
		// constituent views
		CompositeView cv = new CompositeView();

		cv.addConstituentView(new MutationScoreView(mainFrame));
		cv.addConstituentView(new MutationKillSummaryView(mainFrame));
		cv.addConstituentView(new UnKilledMutantInfoView(mainFrame));
		cv.addConstituentView(new RunTimeInfoView(mainFrame));

		cv.displayComponent(output);

		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(new Dimension(1000, 1000));
		mainFrame.setResizable(true);
		mainFrame.setVisible(true);
		
		System.out.println("Done!!");
	}

}
