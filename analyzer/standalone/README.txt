Standalone Analyzer for Major Mutation Framework
ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

The Standalone Analyzer consists of the following components:

|Component  | Description
|___________|__________________
| analyzer | Contains the code files which analyze the mutants and check if they are killed or not.
| data | Store the data such as ids of mutants not killed, runtime of tests etc.
| main | The main class which is to be run.
| output | Provides a JFrame output which displays the results of the mutation analyzer. 
| prePass | A pre-analysis step which will help in prioritizing running of test methods depending on runtime, number of mutants killed etc.
| prioritizer | Determines the order of running the test methods based on information provided by the prePass
| util | Load the jar files and the test classes to be run.

# How to build a release
```
./release/release.sh <version>
```
This script builds all components and the manual, and bundles everything into a
releasable archive. As a sanity check, the provided argument (<version>) must
match the version (as listed in major.properties) of the working directory.

#How to run the standalone analyzer:

The build that is generated on running the release script, has to be unzipped
```
unzip -d/tmp major-1.3.2_jre7.zip

```
The standalone run script must be accessed by using the command
```
cd /tmp/major/example/standalone

```

Following this, the existing standalone example is run by using the command
```
./run.sh

```

This will generate the mutation score of the example test suite for the example source code.

#How the script works
ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
1. The script compiles the source code files using the java compiler provided by Major. 
2. This adds mutants to the code and results in the formation of mutated source code classes. It also generates a mutants.log file which contains the details of all the mutants that have been added to the code.
3. The test classes are compiled.
4. The mutated class files are grouped into one jar file called MutatedClass.jar while the test classes are grouped into Test.jar.
5. The main.class is run and it is provided with 3 arguments containing the entire qualified path to a) The MutatedClass.jar b) Test.jar c) mutants.log. 

#How to run the script on a separate code base
ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
1. To run it on a separate set of source classes and test suite, the code files must be saved in the folders src and test respectively. 
2. Changes will have to be made in the run.sh file to point the major java compiler to the newly added files. 
3. Corresponding changes must be made for the files that are to be added in the Jar files. 
4. After this, the script can be run to generate the mutation score.


#How the analyzer works
ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
As mentioned above, the main class is provided with 3 arguments at run time.

#Setup

1. The code, uses the path of the jar files and with the help of a jar file loader the two jar files are loaded. 
2. A tailored class loader is used to extract all classes relating to test files, the names of which are stored in an array data structure. 
3. Since, when the JUnit test cases are run, they require the source files, we need to load both jar files. 

#PrePass

1. Each JUnit test is run from each test class on each mutated code. 
2. A test method will run on the mutated source code and using Config.java as defined in the config folder of the major build we will be able to get the coverage information.3. Prioritize test cases, favoring cases taking relatively less time to run and covering more mutants.
4. Details of test classes and the corresponding mutants they cover are stored for later optimizations. This information is also used to identify mutants which are not covered at all and no iterations are run for them.
#Mutation Analyzer

1. The mutants.log file is used to get count of the total number of mutants that have been added to the source code files. 2. According to the prioritization order, Mutation analysis is conducted, and mutant kill information is tracked and saved. Also, it stores whether the mutant was killed or there was a timeout.3. If a mutant is killed, no more iterations are run on it. #Output

The output is a JFrame output which displays the final tally of killed mutants, unkilled mutants, killed due to timeout mutants, mutation score. 